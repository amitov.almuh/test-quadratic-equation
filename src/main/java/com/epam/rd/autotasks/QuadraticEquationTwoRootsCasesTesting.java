package com.epam.rd.autotasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class QuadraticEquationTwoRootsCasesTesting {
    protected QuadraticEquation quadraticEquation = new QuadraticEquation();

    private double a;
    private double b;
    private double c;
    private String expected;

    public QuadraticEquationTwoRootsCasesTesting(double a, double b, double c, String expected) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.expected = expected;
    }
    @Parameterized.Parameters
    public static Collection input() {
        return Arrays.asList(new Object[][]{
                {2,5,2,"-0.5 -2.0"},
                {2,5,3,"-1.0 -1.5"},
                {6,9,3,"-0.5 -1.0"},
                {8,12,4,"-0.5 -1.0"}
        });
    }

    @Test
    public void testTwoRootsCase() {
        final Set<String> expectedSet = convertToSet(expected);
        final Set<String> actualSet = convertToSet(quadraticEquation.solve(a, b, c));
        assertEquals(expectedSet, actualSet);
    }

    private Set<String> convertToSet(final String str) {
        final String[] resultArr = str.split(" ");
        return new HashSet<>(Arrays.asList(resultArr));
    }
}
