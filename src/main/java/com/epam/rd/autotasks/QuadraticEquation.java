package com.epam.rd.autotasks;


public class QuadraticEquation {
    public String solve(double a, double b, double c) {
        if (a == 0) {
            throw new IllegalArgumentException();
        } else {
            double dis = Math.pow(b, 2) - 4 * a * c;
            if (dis > 0) {
                String x2 = String.valueOf((Math.sqrt(dis) - b) / (2 * a));
                String x1 = String.valueOf((-Math.sqrt(dis) - b) / (2 * a));
                return (x1 + " " + x2);
            } else if (dis == 0) {
                return String.valueOf(-b / (2 * a));
            } else {
                return ("no roots");
            }
        }
    }
}